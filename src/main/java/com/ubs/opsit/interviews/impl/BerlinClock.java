
package com.ubs.opsit.interviews.impl;

import com.ubs.opsit.interviews.TimeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;

/**
 * Berlin Clock implementation
 *
 * The Berlin Uhr (Clock) is a rather strange way to show the time. On the top of the clock there is a yellow lamp that
 * blinks on/off every two seconds. The time is calculated by adding rectangular lamps.
 *
 * The top two rows of lamps are red. These indicate the hours of a day. In the top row there are 4 red lamps. Every lamp
 * represents 5 hours. In the lower row of red lamps every lamp represents 1 hour. So if two lamps of the first row and
 * three of the second row are switched on that indicates 5+5+3=13h or 1 pm.
 *
 * The two rows of lamps at the bottom count the minutes. The first of these rows has 11 lamps, the second 4. In the
 * first row every lamp represents 5 minutes. In this first row the 3rd, 6th and 9th lamp are red and indicate the first
 * quarter, half and last quarter of an hour. The other lamps are yellow. In the last row with 4 lamps every lamp
 * represents 1 minute.
 *
 * @author Ruslan Khasbulatov / Luxoft
 * @version 1.0
 */
public class BerlinClock implements TimeConverter {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private static final Logger LOG = LoggerFactory.getLogger(BerlinClock.class);

    /**
     *
     * @param aTime usual time in format HH:mm:ss
     * @return Berlin Clock time in format:
     * Y/O (Yellow/Off)
     * [R/O]4 (Red/Off) 4 times
     * [R/O]4 (Red/Off) 4 times
     * [Y/O]2R/O[Y/O]2R/O[Y/O]2R/O[Y/O]2
     * [Y/O]4 (Yellow/Off) 4 times
     */
    @Override
    public String convertTime(String aTime) {
        // null check
        if (aTime == null) {
            return null;
        }
        String result = "";

        // trying to parse date first

        TemporalAccessor time = FORMATTER.parse(aTime);

        long timeSeconds = time.get(ChronoField.SECOND_OF_DAY);

        // top of the clock
        if (timeSeconds % 2 == 0) {
            result += "Y";
        } else {
            result += "O";
        }
        result += LINE_SEPARATOR;

        int timeHours = time.query(LocalTime::from).getHour();
        Period period = time.query(DateTimeFormatter.parsedExcessDays());
        if (period.equals(Period.ofDays(1))) {
            timeHours = 24;
        }

        // top row of four red lamps
        long topRowLampsOn = timeHours / 5;
        result += convertRow(topRowLampsOn, 4 - topRowLampsOn, "R");
        // move to the next row
        result += LINE_SEPARATOR;

        // second row of four red lamps
        long secondRowLampsOn = timeHours % 5;
        result += convertRow(secondRowLampsOn, 4 - secondRowLampsOn, "R");
        // move to the next row
        result += LINE_SEPARATOR;

        long timeMinutes = time.get(ChronoField.MINUTE_OF_HOUR);

        // third row of eleven yellow and red lamps
        long thirdRowLampsOn = timeMinutes / 5;
        result += convertRow(thirdRowLampsOn, 11 - thirdRowLampsOn, "Y").replaceAll("YYY", "YYR");
        // move to the next row
        result += LINE_SEPARATOR;

        // fourth row of eleven yellow and red lamps
        long fourthRowLampsOn = timeMinutes % 5;
        result += convertRow(fourthRowLampsOn, 4 - fourthRowLampsOn, "Y");

        LOG.info(String.format("Berlin Clock time is:\n%s\n", result));
        return result;


    }

    private String convertRow(long lampsOn, long lampsOff, String lampColor) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < lampsOn; i++) {
            result.append(lampColor);
        }
        for (int i = 0; i < lampsOff; i++) {
            result.append("O");
        }
        return result.toString();
    }
}