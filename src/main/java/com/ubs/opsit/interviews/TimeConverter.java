package com.ubs.opsit.interviews;

import java.time.format.DateTimeFormatter;

public interface TimeConverter {

    DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");

    String convertTime(String aTime);

}
